package main

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"time"
)

type card struct {
	name           string
	suit           string
	numericalValue int
	relativeValue  int
}

// type deck []string
type deck []card

func newDeck() deck {
	cards := deck{}

	cardSuits := []string{"Spades", "Diamonds", "Hearts", "Clubs"}
	cardValues := []string{"Ace", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King"}
	cardNumericalValues := []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10}
	cardRelativeValues := []int{14, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13}

	for _, suit := range cardSuits {
		for i, value := range cardValues {
			currentCard := card{
				name:           value + " of " + suit,
				suit:           suit,
				numericalValue: cardNumericalValues[i],
				relativeValue:  cardRelativeValues[i],
			}
			cards = append(cards, currentCard)
		}
	}

	return cards
}

func (d deck) print() {
	for i, card := range d {
		fmt.Println(i, card)
	}
}

func deal(d deck, handSize int) (deck, deck) {
	return d[:handSize], d[handSize:]
}

func (d deck) toString() string {
	for _, card := range d {
		out, err := json.Marshal(card)
		if err != nil {
			panic(err)
		}
		fmt.Println(string(out))
	}
	return "complete"
	// return strings.Join([]string(d), ",")
}

// func (d deck) saveToFile(filename string) error {
// 	return ioutil.WriteFile(filename, []byte(d.toString()), 0666)
// }

// func newDeckFromFile(filename string) deck {
// 	b, err := ioutil.ReadFile(filename)
// 	if err != nil {
// 		fmt.Println("Error:", err)
// 		os.Exit(1)
// 	}
// 	s := strings.Split(string(b), ",")
// 	return deck(s)
// }

func (d deck) shuffle() {

	source := rand.NewSource(time.Now().UnixNano())
	r := rand.New(source)

	for i := range d {
		newPosition := r.Intn(len(d) - 1)
		d[i], d[newPosition] = d[newPosition], d[i]
	}
}
