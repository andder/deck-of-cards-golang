package main

import (
	"testing"
)

func TestNewDeck(t *testing.T) {
	d := newDeck()

	expectedLength := 52
	if len(d) != expectedLength {
		t.Errorf("Expected deck length of %v, but received %v", expectedLength, len(d))
	}

	if d[0].name != "Ace of Spades" {
		t.Errorf("Expected first card to be Ace of Spades, but received %v", d[0])
	}

	if d[len(d)-1].name != "King of Clubs" {
		t.Errorf("Expected last card to be King of Clubs, but received %v", d[len(d)-1])
	}
}

// func TestSaveToFileAndNewDeckFromFile(t *testing.T) {

// 	testFileName := "_decktesting"
// 	expectedLength := 52

// 	os.Remove(testFileName)

// 	d := newDeck()

// 	d.saveToFile(testFileName)

// 	loadedDeck := newDeckFromFile(testFileName)

// 	if len(loadedDeck) != expectedLength {
// 		t.Errorf("Expected deck length of %v, but received %v", expectedLength, len(d))
// 	}

// 	os.Remove(testFileName)
// }
